<?php


namespace App\Presenters;

use Nette;
use Nette\Application\UI;
use Nette\Security\Passwords;
use Nette\Security\User;



/**
 * UsersPresenter class
 */
class UsersPresenter extends Nette\Application\UI\Presenter
{
    //Database variable
    private $database;

    /**
     *  Class constructor init database
     *  @param $database
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * @method renderDefault
     *
     */
    public function renderDefault()
    {


        //Query variable
        $query = $this->database->table('users');

        //Delete user from database
        if($deleteId = $this->getParameter('deleteId'))
        {
            $query->where('id',$deleteId)->delete();
            $this->flashMessage("Uživatel byl úspěšné smazán");
            $this->redirect('Users:');
        }


        //If search is set
        if($this->getParameter('send'))
        {
            $conditionArray = array();

            //Id search
            if ($this->getParameter('user_id')) $query->where('id', $this->getParameter('user_id'));

            //Name search
            if ($this->getParameter('name')) $query->where('name', $this->getParameter('name'));

            //Surname search
            if ($this->getParameter('surname'))  $query->where('surname', $this->getParameter('surname'));

            //Email search
            if ($this->getParameter('email'))  $query->where('email', $this->getParameter('email'));

            //Return count of users
            $numberOfRows = $query->count();


        }
        else
        {
            //Return count of users
            $numberOfRows = $query->count();
        }


        //Set actual page if GET page is NULL then is set first page
        $page = ($this->getParameter('page'))?$this->getParameter('page'):1;


        //Pagination
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($numberOfRows);
        $paginator->setItemsPerPage(10);
        $paginator->setPage($page);

        //Return all users
        $this->template->users = $query->limit($paginator->getLength(), $paginator->getOffset())->order('id DESC');
        //Handover pagination to template
        $this->template->paginator = $paginator;


    }


    /**
     * @method beforeRender
     * Check if user is logged else redirect to login form
     */
    public  function  beforeRender()
    {
        if(!$this->user->isLoggedIn())
        {
            $this->flashMessage("Neautorizovaný přístup. Přihlašte se prosím.");
            $this->redirect('Homepage:');
        }
    }

    /**
     * @method createComponentSearchInUsers
     * Component which create search form
     * @return Form
     */
    function createComponentSearchInUsers()
    {
        $form = new UI\Form;
        $form->addText('user_id');
        $form->addText('name');
        $form->addText('surname');
        $form->addText('email');
        $form->addSubmit('send',"Hledat");
        $form->setMethod('get');
        return $form;
    }


    /**
     * @method createComponentAddUser
     * Component which create add user form
     * @return Form
     */
    protected function createComponentAddUser()
    {

        $form = new UI\Form;
        $form->addText('name', 'Jméno:')->setRequired('Zadejte prosím jméno');
        $form->addText('surname', 'Příjmení:')->setRequired('Zadejte prosím příjmení');
        $form->addText('email', 'Email:')->setRequired('Zadejte prosím email');
        $form->addPassword('password', 'Heslo:');
        $form->addSubmit('create', 'Vytvořit');
        $form->onSuccess[] = [$this, 'createFormSucceeded'];
        return $form;
    }

    /**
     * @method createFormSucceeded
     * @param $form
     * @param $values sended values from form
     * Add or edit user function
     */
    public function createFormSucceeded($form, $values)
    {


        //Check if GET userId is set
        if ($userId = $this->getParameter('userId'))
        {
            //Check if is send password value from form
            if(!$values->password)
            {
                //then unset from $values
                unset($values->password);
            }
            //else hash password
            else
            {
                $values->password = Passwords::hash($values->password);
            }

            //update user
            $this->database->table('users')->get($userId)->update($values);


            $this->flashMessage("Uživatel {$values->name} {$values->surname} byl úspěšné upraven");
        }
        //else create new user
        else
        {
            //Insert new user to Db
            $this->database->table('users')->insert([
                'name' => $values->name,
                'surname' => $values->surname,
                'email' => $values->email,
                'password' => Passwords::hash($values->password),
            ]);

            $this->flashMessage("Uživatel {$values->name} {$values->surname} byl úspěšné vytvořen");
        }

        //Redirect to users list
        $this->redirect('Users:');



    }


    /**
     * @method actionEdit
     * @param int $userId
     * Edit user method
     */
    public function actionEdit($userId)
    {
        $users = $this->database->table('users')->get($userId);

        if (!$users)
        {
            $this->error('Uživatel nebyl nalezen');
        }

        $this['addUser']->setDefaults($users->toArray());
    }

    /**
     * @method handleLogout
     * User logout
     */
    public function handleLogout()
    {

            $this->user->logout();
            $this->redirect('Homepage:');


    }

}