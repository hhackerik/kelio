<?php

namespace App\Presenters;
use Nette\Security as NS;
use Nette;
use App\Model;
use Nette\Security\Identity;
use Nette\Application\UI;
use Nette\Security\User;
use Nette\Security\Passwords;


/**
 * HomepagePresenter class
 */
class HomepagePresenter extends BasePresenter
{
	//Database variable
	private $database;

	/**
	 *  Class constructor init database
	 *  @param $database
	 */
	public function  __construct(Nette\Database\Context $database)
	{

		$this->database = $database;
	}

	/**
	 * @method createComponentRegistrationForm
	 * Create sing up form
	 */
	protected function createComponentSingUpForm()
	{
		$form = new UI\Form;
		$form->addText('email', 'Email:')->setRequired('Zadejte prosím email');
		$form->addPassword('password', 'Heslo:')->setRequired('Zadejte prosím heslo');
		$form->addSubmit('login', 'Přihlásit');
		$form->onSuccess[] = [$this, 'singUpFormSucceeded'];
		return $form;
	}

	/**
	 * @method singUpFormSucceeded
	 * User SingUp
	 */
	public function singUpFormSucceeded(UI\Form $form, $values)
	{
		//Check if user exists
		if($query = $this->database->table('users')->where('email', $values->email)->count())
		{
			$this->flashMessage('Byl jste úspěšně přihlášen.');

			//Set user
			$this->user->login($values->email, $values->password);

		}


	}

	/**
	 * @method handleLogout
	 * User logout
	 */
	public function handleLogout()
	{
			$this->user->logout();
			$this->redirect('Homepage:');
	}




}
